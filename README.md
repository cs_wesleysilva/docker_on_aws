README
======

Brief description
-----------------

This is a set of Ansible Playbooks which intend to setup a new Docker Host server running on an AWS EC2 instance.

The operating system chosen is the Amazon Linux (I am using the latest AMI ID available in june 2015) and two General Purpose SSD disks are being used:

 - the first disk is the default root, mounted as /. 
 - the second disk is formated using BTRFS and mounted as /var/lib/docker.

The Playbooks
-------------

There are two playbook files. You must obbey the numeric sequence of execution.

The first will manage the Amazon demands: create a keypair, create the EC2 instance, attach the EBS and start the system.

The second will format and mount the EBS, install Docker and some useful applications: Apache to create a reverse proxy (useful to expose multiple containers HTTP ports using Virtual Hosts), Git to clone your projects and build your containers, and some development libraries needed to compile and build software dependencies for containers.

